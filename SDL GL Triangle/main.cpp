// Based loosly on the first triangle OpenGL tutorial
// http://www.opengl.org/wiki/Tutorial:_OpenGL_3.1_The_First_Triangle_%28C%2B%2B/Win%29
// This program will render two triangles
// Most of the OpenGL code for dealing with buffer objects, etc has been moved to a 
// utility library, to make creation and display of mesh objects as simple as possible

// Windows specific: Uncomment the following line to open a console window for debug output
#if _DEBUG
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif

#include "rt3d.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <stack>
using namespace std;

// Globals
// Real programs don't use globals :-D
// Data would normally be read from files
std::stack<glm::mat4> mvStack;
GLfloat rotY,rotX;
GLfloat f;
GLfloat rot;
GLfloat savedRot;
bool paused;
int numPlanets = 9;
int numMoons[] = {0,0,1,2,10,10,6,4,2};
rt3d::lightStruct light0 = {
	{0.2f,0.2f,0.2f,1.0f},	//ambient
	{0.7f,0.2f,0.2f,1.0f},	//diffuse
	{0.8f,0.8f,0.8f,1.0f},	//specular
	{0.0f,0.0f,-15.0f,1.0f}	//position
};
rt3d::materialStruct material0 = {
	{0.4f,0.2f,0.2f,1.0f},	//ambient
	{0.8f,0.5f,0.5f,1.0f},	//diffuse
	{1.0f,0.8f,0.8f,1.0f},	//specular
	2.0f					//shininess
};
rt3d::materialStruct material1 = {
	{0.5f,0.5f,0.5f,1.0f},	//ambient
	{0.2f,0.5f,0.5f,1.0f},	//diffuse
	{1.0f,1.0f,1.0f,1.0f},	//specular
	50.0f					//shininess
};
rt3d::materialStruct material2 = {
	{0.0f,1.0f,0.0f,1.0f},	//ambient
	{0.0f,1.0f,0.0f,1.0f},	//diffuse
	{1.0f,1.0f,0.0f,1.0f},	//specular
	25.0f					//shininess
};
int modeSelector;
GLuint cubeVertCount = 36;
GLuint cubeIndexCount = 36;
GLuint cubeIndices[] = {	0,1,2,0,2,3,//back
							1,0,5,0,4,5,//left
							6,3,2,3,6,7,//right
							1,5,6,1,6,2,//top
							0,3,4,3,7,4,//bottom
							6,5,4,7,6,4};//front

GLfloat cubeVertices[] = {	-0.5f,-0.5f,-0.5f,
							-0.5f,0.5f,-0.5f,
							0.5f,0.5f,-0.5f,
							0.5f,-0.5f,-0.5f,
							-0.5f,-0.5f,0.5f,
							-0.5f,0.5f,0.5f,
							0.5f,0.5f,0.5f,
							0.5f,-0.5f,0.5f,};

GLfloat colours[] = {	0.0f, 0.0f, 0.0f,
						0.0f, 1.0f, 0.0f,
						1.0f, 1.0f, 0.0f,
						1.0f, 0.0f, 0.0f,
						0.0f, 0.0f, 1.0f,
						0.0f, 1.0f, 1.0f,
						1.0f, 1.0f, 1.0f,
						1.0f, 0.0f, 1.0f};

GLuint shaderProgram;

GLuint meshObjects[2];
float dx,dy,dz;


// Set up rendering context
SDL_Window * setupRC(SDL_GLContext &context) {
	SDL_Window * window;
    if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
        rt3d::exitFatalError("Unable to initialize SDL"); 
	  
    // Request an OpenGL 3.0 context.
    // Not able to use SDL to choose profile (yet), should default to core profile on 3.2 or later
	// If you request a context not supported by your drivers, no OpenGL context will be created

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE); 

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  // double buffering on
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4); // Turn on x4 multisampling anti-aliasing (MSAA)
 
    // Create 800x600 window
    window = SDL_CreateWindow("SDL/GLM/OpenGL Demo", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        800, 600, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
	if (!window) // Check window was created OK
        rt3d::exitFatalError("Unable to create window");
 
    context = SDL_GL_CreateContext(window); // Create opengl context and attach to window
    SDL_GL_SetSwapInterval(1); // set swap buffers to sync with monitor's vertical refresh rate
		glEnable(GL_DEPTH_TEST);
	return window;
}


void init(void) {
	// For this simple example we'll be using the most basic of shader programs

	shaderProgram=rt3d::initShaders("phong.vert","phong.frag");
	rt3d::setLight(shaderProgram,light0); 
	rt3d::setMaterial(shaderProgram,material0);
	f = 45.0f;
	// Going to create our mesh objects here
	meshObjects[0] = rt3d::createMesh(cubeVertCount,cubeVertices,nullptr,cubeVertices,nullptr,cubeIndexCount,cubeIndices);

}
void update(void) 
{
	//get keyboard state buffer and adjust transform values accordingly.
	Uint8 *keys = SDL_GetKeyboardState(NULL);
	if ( keys[SDL_SCANCODE_UP]) rotX+=0.5;
	if ( keys[SDL_SCANCODE_DOWN]) rotX-=0.5;
	if ( keys[SDL_SCANCODE_LEFT]) rotY+=0.5;
	if ( keys[SDL_SCANCODE_RIGHT]) rotY-=0.5;
	if ( keys[SDL_SCANCODE_A] ) dx += 0.05;
	if ( keys[SDL_SCANCODE_D] ) dx -= 0.05;
	if ( keys[SDL_SCANCODE_W] ) dz += 0.05;
	if ( keys[SDL_SCANCODE_S] ) dz -= 0.05;
	if ( keys[SDL_SCANCODE_0] ) modeSelector = 0;
	if ( keys[SDL_SCANCODE_1] ) modeSelector = 1;
	if ( keys[SDL_SCANCODE_RETURN]) paused = true;
	if ( keys[SDL_SCANCODE_BACKSPACE]) paused = false;
	

}
void draw(SDL_Window * window) {
	if(!paused)
	rot++;
	// clear the screen
	glClearColor(0.0f,0.0f,0.0f,1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//for each planet loop
	//for each moon loop
	//set identity
	//push moon
	// translate item
	// rotate item
	// scale item
	// render moon
	//pop moon
	//end loop
	//set identity
	//push planet
	// translate item
	// rotate item
	// scale item
	//render planet
	//pop planet
	//end loop


	//set projection and modeview matrices to identity.

	glm::mat4 projection(1.0);

	projection = glm::perspective(f,800.0f/600.0f,1.0f,120.0f);
	rt3d::setUniformMatrix4fv(shaderProgram, "projection", glm::value_ptr(projection));

	//render sun
	glm::mat4 modelview(1.0);
	//begin performing transformations
	mvStack.push(modelview);
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0, 0, -15.0f));
	mvStack.top() = glm::rotate(mvStack.top(),rotX,glm::vec3(1.0f,0.0f,0.0f));
	mvStack.top() = glm::rotate(mvStack.top(),rotY,glm::vec3(0.0f,0.0f,1.0f));
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(dx, dy, dz));
	mvStack.push(mvStack.top());
	
	mvStack.top() = glm::rotate(mvStack.top(),rot,glm::vec3(0.0,0.0,1.0));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(1.0,1.0,1.0));
	//glm::mat4 MVP = projection * mvStack.top();
	rt3d::setMaterial(shaderProgram,material0);
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	//send command to draw either in wire frame mode or full render
	if(!modeSelector)
	rt3d::drawIndexedMesh(meshObjects[0],cubeIndexCount,GL_TRIANGLES); //polygon view
	else
	rt3d::drawIndexedMesh(meshObjects[0],cubeIndexCount,GL_LINE_LOOP); //wireframe view
	mvStack.pop();
	
	for (size_t i =0;i<numPlanets;i++)
	{
		rt3d::setMaterial(shaderProgram,material1);
		GLfloat orbitalDistance = 3.0f*(1+i);
		GLfloat rotSpeed = rot/(orbitalDistance*orbitalDistance);
		mvStack.push(mvStack.top());
		mvStack.top() = glm::rotate(mvStack.top(),rot/orbitalDistance,glm::vec3(0.0f,0.0f,1.0f));
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(orbitalDistance, 0.0f, 0.0f));

		mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.5,0.5,0.5));
		mvStack.top() = glm::rotate(mvStack.top(),rot,glm::vec3(0.0f,0.0f,1.0f));

		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));

		//send command to draw either in wire frame mode or full render
		if(!modeSelector)
		rt3d::drawIndexedMesh(meshObjects[0],cubeIndexCount,GL_TRIANGLES); //polygon view
		else
		rt3d::drawIndexedMesh(meshObjects[0],cubeIndexCount,GL_LINE_LOOP); //wireframe view
		size_t moons = numMoons[i];
		if(moons!=0);
		for (size_t j = 0; j < moons;j++)
		{
			rt3d::setMaterial(shaderProgram,material2);
			GLfloat moonOrbit = 1.0f*(j+1.0f);
			rotSpeed = rot/(moonOrbit*moonOrbit);
			mvStack.push(mvStack.top());
			
			mvStack.top() = glm::rotate(mvStack.top(),rot/moonOrbit,glm::vec3(0.0f,0.0f,1.0f));
			mvStack.top() = glm::translate(mvStack.top(), glm::vec3(moonOrbit,0.0f, 0.0));
			mvStack.top() = glm::rotate(mvStack.top(),rot,glm::vec3(0.0f,0.0f,1.0f));
	
			mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.5f,0.5f,0.5f));
			rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));

			//send command to draw either in wire frame mode or full render
			if(!modeSelector)
			rt3d::drawIndexedMesh(meshObjects[0],cubeIndexCount,GL_TRIANGLES); //polygon view
			else
			rt3d::drawIndexedMesh(meshObjects[0],cubeIndexCount,GL_LINE_LOOP); //wireframe view
			mvStack.pop();
		}
		mvStack.pop();

	}

	
	mvStack.pop();

    SDL_GL_SwapWindow(window); // swap buffers
}


// Program entry point - SDL manages the actual WinMain entry point for us
int main(int argc, char *argv[]) {
    SDL_Window * hWindow; // window handle
    SDL_GLContext glContext; // OpenGL context handle
    hWindow = setupRC(glContext); // Create window and render context 

	// Required on Windows *only* init GLEW to access OpenGL beyond 1.1
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err) { // glewInit failed, something is seriously wrong
		std::cout << "glewInit failed, aborting." << endl;
		exit (1);
	}
	cout << glGetString(GL_VERSION) << endl;
	//initialize variables
	init();

	bool running = true; // set running to true
	SDL_Event sdlEvent;  // variable to detect SDL events
	while (running)	{	// the event loop
		while (SDL_PollEvent(&sdlEvent)) {
			if (sdlEvent.type == SDL_QUIT)
				running = false;
			else
			{
				update();
			}
		}
		draw(hWindow); // call the draw function
	}

    SDL_GL_DeleteContext(glContext);
    SDL_DestroyWindow(hWindow);
    SDL_Quit();
    return 0;
}