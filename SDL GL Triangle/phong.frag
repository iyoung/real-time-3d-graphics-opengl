// Phong fragment shader phong2.frag
// matched with phong2.vert
#version 130

// Some drivers require the following
precision highp float;

struct lightStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
};

struct materialStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
};

uniform lightStruct light;
uniform materialStruct material;

in vec3 ex_N;
in vec3 ex_V;
in vec3 ex_L;
out vec4 out_Color;
 
void main(void) {
    
	// Ambient intensity
	vec4 ambientI = light.ambient * material.ambient;

	// Diffuse intensity
	vec4 diffuseI = light.diffuse * material.diffuse * max(dot(ex_N,ex_L),0);

	// Specular intensity
	// Calculate R - reflection of light
	vec3 R = normalize(-reflect(ex_L,ex_N));
	vec4 specularI = light.specular * material.specular;
	specularI = specularI * pow(max(dot(R,ex_V),0), material.shininess);

	// Fragment colour
	out_Color = ambientI + diffuseI + specularI;
}